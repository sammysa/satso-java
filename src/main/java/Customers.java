package za.co.prologiq;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
@Entity
@Table(name =  "prologiq_satso17", schema = "customer_information")
public class Customers implements java.io.Serializable {

    private Integer id;
    private String name;
    private String surname;
    private String email;
    private String home_address;
    private String contact_num;
    private date cust_dob;



    public Customers(Integer id, String name, String surname,
                     String email, String home_address, String contact,
                     date custDOB)
    {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.home_address = home_address;
        this.contact_num = contact;
        this.cust_dob = custDOB;
    }

    /**
     * Getters and Setters
     * @return
     */
    @Id
    @Column(name = "id")
    public Integer getId()
    {
        return this.id;
    }
    public String getName()
    {
        return this.name;
    }

    public String getSurname()
    {
        return this.surname;
    }

    public String getEmail()
    {
        return this.email;
    }

    public String getHome_address()
    {
        return this.home_address;
    }

    public String getContact_num()
    {
        return this.contact_num;
    }

    public date getCust_dob()
    {
        return this.cust_dob;
    }
}
