package za.co.prologiq.rest;

import za.co.prologiq.Customers;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@javax.inject.Singleton
@Path("Customers")
public class CustomersFacadeREST extends AbstractFacade<Customers>{
    private EntityManager em;

    public CustomersFacadeREST()
    {
        super(Customers.class);
    }

    @PUT
    @Override
    @Consumes({"application/xml", "application/json"})
    public Response edit(Customers entity)
    {
        if(entity.getName().length() <= 3)
        {
            return Response.status(Response.Status.CONFLICT).entity("Customer name is too short").type(MediaType.TEXT_PLAIN).build();
        }

        return super.edit(entity);
    }

    @DELETE
    @Path("remove/{id}")
    public Response remove(@PathParam("id") Integer id)
    {
        return super.remove(super.find(id));
    }

    @GET
    @Override
    @Produces({"application/json"})
    public List<Customers> findAll()
    {

        return super.findAll();
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public Customers find(@PathParam("id") Integer id)
    {
        return super.find("id");
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Customers> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to)
    {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST()
    {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager()
    {
        em = ENtityManagerHelper.getEntityManager();
        return em;
    }




}
